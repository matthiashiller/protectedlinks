<?php
/**
 * Protected Links plugin for Craft CMS 3.x
 *
 * Secure & restricted files download
 *
 * @link      http://www.intoeetive.com/
 * @copyright Copyright (c) 2018 Yurii Salimovskyi
 */

namespace intoeetive\protectedlinks\models;

use intoeetive\protectedlinks\ProtectedLinks;

use Craft;
use craft\base\Model;

/**
 * Link Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Yurii Salimovskyi
 * @package   ProtectedLinks
 * @since     0.0.1
 */
class Link extends Model
{
    // Public Properties
    // =========================================================================

    public int $siteId = 1;
    public int $assetId = 0;
    public string $checksum = '';
    public int $denyHotlink = 0;
    public int $requireLogin = 0;
    public string $memberGroups = '';
    public string $members = '';
    public int $inline = 0;
    public int $mimeType = 0;
    public int $downloads = 0;

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            ['assetId', 'integer']
        ];
    }
}
